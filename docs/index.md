# Python 정규 표현식: 검색, 일치, 교체 <sup>[1](#footnote_1)</sup>

<font size="3">Python에서 패턴 매칭과 텍스트 조작을 위해 정규 표현식(regular expression)을 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./regular-expressions.md#intro)
1. [정규 표현식이란?](./regular-expressions.md#sec_02)
1. [Python에서 `re` 모듈 사용법](./regular-expressions.md#sec_03)
1. [기본 구문과 메타문자](./regular-expressions.md#sec_04)
1. [일반적인 메서드와 속성](./regular-expressions.md#sec_05)
1. [그룹과 캡처](./regular-expressions.md#sec_06)
1. [플래그와 수식어](./regular-expressions.md#sec_07)
1. [고급 패턴과 기법](./regular-expressions.md#sec_08)
1. [어플리케이션과 예](./regular-expressions.md#sec_09)
1. [요약](./regular-expressions.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 25 — Python Regular Expressions: Search, Match, Replace](https://levelup.gitconnected.com/python-tutorial-25-python-regular-expressions-search-match-replace-6f8cab16958e?sk=8bff1719f352ef3e8dfe575cb58e9f18)를 편역하였다.
