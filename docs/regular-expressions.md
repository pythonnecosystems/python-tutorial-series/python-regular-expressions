# Python 정규 표현식: 검색, 일치, 교체

## <a name="intro"></a> 개요
Python 정규 표현식에 대한 이 포스팅에서는 Python에서 패턴 매칭과 텍스트 조작에 정규 표현식을 사용하는 방법을 배울 것이다.

정규 표현식은 텍스트 데이터를 찾고, 추출하고, 대체하고, 검증하는 강력한 도구이다. 정규 표현식은 데이터 분석, 웹 개발, 텍스트 처리 및 자연어 처리 같은 많은 분야에서 널리 사용된다.

Python에는 정규 표현식으로 작업할 수 있는 풍부한 함수와 방법을 제공하는 `re`라는 모듈이 내장되어 있다. 복잡한 패턴과 규칙을 사용하여 텍스트를 검색, 일치, 대체 및 분할할 수 있다.

이 글에서는 다음과 같은 내용을 배울 수 있다.

- 정규 표현식은 무엇이며 작동 방식은 어떠한가?
- Python에서 `re` 모듈 사용법
- 기본 및 고급 정규 표현식 패턴 작성 방법
- `re` 모듈의 일반적인 메서드와 속성 사용 방법
- 그룹과 캡처를 사용하여 하위 일치 추출 방법
- 플래그와 수식어를 사용하여 정규 표현식의 동작 제어 방법
- 다양한 작업과 문제에 정규 표현식 적용 방법

이 글을 읽고나면 Python 프로젝트에서 정규 표현식을 자신 있게 효과적으로 사용할 수 있다.

정규 표현식의 세계로 뛰어들 준비가 되었나요? 시작해 보자!

## <a name="sec_02"></a> 정규 표현식이란?
정규 표현식은 검색 패턴을 정의하는 문자의 시퀀스이다. 정규 표현식을 사용하여 특정 규칙과 기준에 따라 텍스트를 찾고, 일치시키고, 교체하거나, 분할할 수 있다.

정규 표현식은 리터럴과 메타문자의 두 가지 유형의 문자로 구성되어 있다. 리터럴은 문자, 숫자, 기호와 같이 그 자체와 일치하는 일반적인 문자이다. 메타문자는 와일드카드(wildcard), 수량화자(quantiifier), 앵커(amchor), 수식어(modifier)와 같이 특별한 의미와 기능을 가진 특별한 문자이다.

예를 들어, 정규 표현식 `cat`은 "The cat is on the mat" 텍스트에서 문자 그대로의 문자열 "cat"과 일치한다. 정규 표현식 `c.t`는 "cat", "cot", "cut" 또는 "c-t"와 같이 "c"으로 시작하고 "t"로 끝나는 임의의 3자 문자열과 일치한다. 점(`.`)은 임의의 단일 문자와 일치하는 메타 문자이다.

정규 표현식은 복잡한 패턴과 규칙을 만들기 위해 서로 다른 메타문자를 결합할 수 있기 때문에 매우 강력하고 유연하다. 예를 들어, 정규 표현식 `c[aou]t`는 "c"로 시작하여 "t"로 끝나는 문자열과 일치하고 "cat", "cot" 또는 "cut"처럼 사이에 "a", "o" 또는 "u"가 있다. 대괄호(`[]`)는 괄호 안의 문자 중 하나와 일치하는 문자 클래스를 정의하는 메타문자이다.

정규 표현식은 Python, Java, Perl, grep, sed와 awk 같은 많은 프로그래밍 언어와 도구에서 널리 사용된다. 또한 NotePad++, Sublime Text 와 Visual Studio Code 같은 많은 텍스트 편집기에서도 지원한다. 정규 표현식을 사용하여 텍스트 데이터에 대한 다양한 작업과 동작을 수행할 수 있다.

- 키워드 또는 구문 찾기와 강조 표시
- 정형 또는 비정형 텍스트에서 정보 추출과 구문 분석
- 사용자 입력 또는 출력의 유효성 확인과 형식 지정
- 원하지 않거나 중복된 텍스트 대체 또는 삭제
- 구분자 또는 구분자를 기준으로 텍스트 분할 또는 결합

[다음 절](#sec_03)에서는 Python에서 `re` 모듈을 사용하여 정규 표현식을 사용하는 방법에 대해 알아본다.

## <a name="sec_03"></a> Python에서 `re` 모듈 사용법
Python에서 정규 표현식을 사용하려면 `re` 모듈을 임포트해야 한다. `re` 모듈은 정규 표현식을 사용하여 텍스트에 대한 다양한 연산을 수행할 수 있는 일련의 함수와 메서드를 제공한다.

모듈의 가장 일반적인 함수과 메서드는 다음과 같다.

- **`re.search(pattern, string, flags=0)`**: 이 함수는 문자열에서 패턴이 처음 발생하는 것을 검색하여 발견되면 일치 객체를 반환하거나 발견되지 않으면 `None`을 반환한다. 일치 객체에는 시작과 끝 위치, 값과 캡처된 그룹 같은 일치하는 하위 문자열에 대한 정보를 포함한다. 선택적 `flags` 인수를 사용하여 대소문자 무시 또는 다중 줄 일치 허용과 같은 정규 표현식의 동작을 수정할 수 있다.
- **`re.match(pattern, string, flags=0)`**: 이 함수는 `re.search`와 유사하지만 문자열의 시작에서 일치하는 패턴만을 찾는다. 문자열의 시작 부분에 패턴이 있으면 일치 객체를 반환하거나 그렇지 않으면 `None`을 반환한다.
- **`re.findall(pattern, string, flags=0)`**: 이 함수는 문자열에서 패턴의 겹치지 않는 모든 일치 항목의 리스트를 반환한다. 일치 객체는 반환하지 않고 일치된 하위 문자열들만 반환한다. 패턴에 그룹이 포함된 경우에는 튜플 리스트를 반환하며, 여기서 각 튜플에는 일치된 그룹이 포함된다.
- **`re.finditer(pattern, string, flags=0)`**: 이 함수는 문자열 내 패턴의 겹치지 않는 모든 일치 항목에 대한 일치 객체의 반복자를 반환한다. `re.findall`과 유사하지만 일치 항목을 반복하고 일치 객체의 속성을 액세스할 수 있다.
- **`re.sub(pattern, repl, string, count=0, flags=0)`**: 이 함수는 문자열에서 발생하는 모든 패턴을 대체 문자열 또는 함수로 대체한다. 옵션의 `count` 인수는 대체할 최대 수를 지정하고 옵션의 `flags` 인수는 정규 표현식의 동작을 수정한다.
- **`re.split(pattern, string, maxsplit=0, flags=0)`**: 이 함수는 패턴을 구분 기호로 사용하여 문자열을 하위 문자열 목록으로 분할한다. 옵션의 `maxsplit` 인수는 분할할 최대 수를 지정하고 옵션의 `flags` 인수는 정규 표현식의 동작을 수정한다.
- **`re.compile(pattern, flags=0)`**: 이 함수는 위의 작업을 보다 효율적으로 수행하는 데 사용할 수 있는 패턴을 정규 표현식 객체로 컴파일한다. 정규 표현식 객체는 `search`, `match`, `findall`, `sub`와 `split` 같은 `re` 모듈과 동일한 메서드를 갖는다. 옵션의 `flags` 인수는 정규 표현식의 동작을 수정한다.

Python에서 `re` 모듈을 사용하는 몇 가지 예는 다음과 같다.

```python
import re
# Define a string
text = "Python is a great programming language."
# Define a pattern
pattern = "Python"
# Search for the pattern in the string
result = re.search(pattern, text)
# Check if the pattern is found
if result:
    # Print the matched substring
    print(result.group())
    # Print the start and end positions of the match
    print(result.start(), result.end())
else:
    # Print a message if the pattern is not found
    print("No match found.")
# Output:
# Python
# 0 6
```

## <a name="sec_04"></a> 기본 구문과 메타문자
이 절에서는 기본적인 정규 표현 패턴을 작성하는 방법과 메타문자(metacharacter)를 사용하여 보다 복잡하고 유연한 패턴을 만드는 방법을 배운다.

정규 표현식 패턴은 검색 기준을 정의하는 문자의 시퀀스이다. 문자와 메타문자를 사용하여 패턴을 구성할 수 있다.

문자는 문자, 숫자와 기호 같은 문자 자체와 일치하는 일반적인 문자이다. 예를 들어, 패턴 `cat`은 "The cat is on the mat" 텍스트에서 문자 그대로의 문자열 "cat"과 일치한다.

메타문자는 와일드카드, 수량화자, 앵커, 수식어와 같이 패턴에 특별한 의미와 기능을 가진 특별한 문자이다. 다양한 문자와 일치시키고, 반복 횟수를 지정하고, 위치를 나타내며, 패턴의 동작을 수정할 수 있다. 예를 들어, 패턴 `c.t`는 "cat", "cot", "cut" 또는 "c-t"와 같이 "c"로 시작하고 "t"로 끝나는 임의의 3자 문자열과 일치한다. 점(`.`)은 임의의 단일 문자와 일치하는 메타문자이다.

다음은 가장 일반적인 메타문자와 그 의미를 나열한 목록이다,

![](./1_ifBGKCT37tbv5Tk505_I9w.webp)

`.`은  newline(\n)을 제외한 단일 문자와, `[]` 안에 포함된 문자 중 하나와, `[^]` 안에 포함되지 않은 문자 중 하나와 일치한다., `*` 이전 문자와 일치 횟수가 0회 이상인 그룹, `+`이전 문자와 일치 횟수가 1회 이상인 그룹, `?`이전 문자와 일치 횟수가 0 또는 1회을 찾는다. `{}` 이전 문자 또는 그룹의 일치 횟수를  지정한다. `()` 하위 패턴을 그룹화하여 나중에 사용할 수 있도록 캡처한다. `\`Escape 특수 문자를 사용하거나 특수 시퀀스를 나타낸다. `^`는 문자열 또는 행의 시작 부분과 일치시킨다. `$` 문자열 또는 라인의 끝 부분과 일치시킨다. `|`는 왼쪽 또는 오른쪽 피연산자 중 하나와 일치시킨다

다음은 메타문자를 사용하여 정규 표현식 패턴을 만드는 예이다.

```python
# Import the re module
import re
# Define a string
text = "The cat is on the mat. The dog is on the log."
# Define a pattern with metacharacters
pattern = "[cd][ao][gt]"
# Find all matches of the pattern in the string
result = re.findall(pattern, text)
# Print the result
print(result)
# Output:
# ['cat', 'dog']
```

## <a name="sec_05"></a> 일반적인 메서드와 속성
이 절에서는 Python에서 모듈과 일치 객체의 일반적인 메서드와 속성 중 사용하는 방법 일부를 배운다.

`re` 모듈은 정규 표현식을 사용하여 텍스트에 대한 다양한 연산을 수행할 수 있는 여러 가지 메서드를 제공한다. 가장 일반적인 방법 중 일부는 다음과 같다.

- **`re.search(pattern, string, flags=0)`**: 이 메서드는 문자열에서 패턴이 처음 발생하는 것을 검색하여 발견되면 일치 객체를 반환하거나 발견되지 않으면 `None`을 반환한다. 일치 객체에는 시작과 끝 위치, 값과 캡처된 그룹 같은 일치하는 하위 문자열에 대한 정보를 포함한다. 선택적 `flags` 인수를 사용하여 대소문자 무시 또는 다중 줄 일치 허용과 같은 정규 표현식의 동작을 수정할 수 있다.
- **`re.match(pattern, string, flags=0)`**: 이 메서드는 `re.search`와 유사하지만 문자열의 시작에서 일치하는 패턴만을 찾는다. 문자열의 시작 부분에 패턴이 있으면 일치 객체를 반환하거나 그렇지 않으면 `None`을 반환한다.
- **`re.findall(pattern, string, flags=0)`**: 이 메서드는 문자열에서 패턴의 겹치지 않는 모든 일치 항목의 리스트를 반환한다. 일치 객체는 반환하지 않고 일치된 하위 문자열들만 반환한다. 패턴에 그룹이 포함된 경우에는 튜플 리스트를 반환하며, 여기서 각 튜플에는 일치된 그룹이 포함된다.
- **`re.finditer(pattern, string, flags=0)`**: 이 메서드는 문자열 내 패턴의 겹치지 않는 모든 일치 항목에 대한 일치 객체의 반복자를 반환한다. `re.findall`과 유사하지만 일치 항목을 반복하고 일치 객체의 속성을 액세스할 수 있다.
- **`re.sub(pattern, repl, string, count=0, flags=0)`**: 이 메서드는 문자열에서 발생하는 모든 패턴을 대체 문자열 또는 함수로 대체한다. 옵션의 `count` 인수는 대체할 최대 수를 지정하고 옵션의 `flags` 인수는 정규 표현식의 동작을 수정한다.
- **`re.split(pattern, string, maxsplit=0, flags=0)`**: 이 메서드는 패턴을 구분 기호(delimeter)로 사용하여 문자열을 하위 문자열 목록으로 분할한다. 옵션의 `maxsplit` 인수는 분할할 최대 수를 지정하고 옵션의 `flags` 인수는 정규 표현식의 동작을 수정한다.

`re.search`와 `re.match`가 반환하는 `match` 객체에는 일치에 대한 정보를 액세스할 수 있는 몇 가지 속성과 메서드가 있다. 가장 일반적인 속성과 메서드는 다음과 같다.

- **`match.group()`**: 이 메서드는 일치하는 부분 문자열을 반환한다. 패턴에 그룹이 포함된 경우 메서드에 인덱스 또는 이름을 전달하여 특정 그룹을 가져올 수 있다. 예를 들어 `match.group(1)`은 첫 번째 그룹을 반환하고 `match.group('name')`은 'name'이라는 이름의 그룹을 반환한다.
- **`match.groups()`**: 이 메서드는 일치하는 모든 그룹의 튜플을 반환한다. 패턴에 그룹이 없으면 빈 튜플을 반환한다.
- **`match.groupdict()`**: 이 메서드는 이름이 지정된 모든 그룹과 해당 값의 사전을 반환한다. 패턴에 이름이 지정된 그룹이 없으면 빈 사전을 반환한다.
- **`match.start()`**: 이 메서드는 일치하는 부분 문자열의 시작 위치를 반환한다. 패턴에 그룹이 포함된 경우 메서드에 인덱스 또는 이름을 전달하여 특정 그룹의 시작 위치를 얻을 수 있다.
- **`match.end()`**: 일치하는 부분 문자열의 끝 위치를 반환하는 메서드이다. 패턴에 그룹이 포함된 경우 메서드에 인덱스 또는 이름을 전달하여 특정 그룹의 끝 위치를 얻을 수 있다.
- **`match.span()`**: 이 메서드는 일치하는 부분 문자열의 시작 위치와 끝 위치의 튜플을 반환한다. 패턴에 그룹이 포함된 경우 메서드에 인덱스 또는 이름을 전달하여 특정 그룹의 스팬을 가져올 수 있다.

Python에서 `re` 모듈과 `match` 객체의 메소드와 속성을 사용하는 예는 다음과 같다.

```python
# Import the re module
import re
# Define a string
text = "The cat is on the mat. The dog is on the log."
# Define a pattern with groups
pattern = "(\w+) is on the (\w+)"
# Search for the pattern in the string
result = re.search(pattern, text)
# Check if the pattern is found
if result:
    # Print the matched substring
    print(result.group())
    # Print the first group
    print(result.group(1))
    # Print the second group
    print(result.group(2))
    # Print the start and end positions of the match
    print(result.start(), result.end())
    # Print the span of the match
    print(result.span())
else:
    # Print a message if the pattern is not found
    print("No match found.")
# Output:
# cat is on the mat
# cat
# mat
# 4 19
# (4, 19)
```

## <a name="sec_06"></a> 그룹과 캡처
이 절에서는 그룹과 캡처를 사용하여 정규 표현식 일치에서 하위 일치를 추출하는 방법에 대해 알아본다.

그룹은 정규 표현식 내에서 하위 패턴을 그룹화하는 방법이다. 괄호 `()`를 사용하여 그룹을 만들 수 있다. 예를 들어 `(\w+) is on the (\w+)` 패턴은 두 개의 그룹을 갖고 있다. 첫 번째 그룹은 하나 이상의 단어와 일치하고 두 번째 그룹은 단어 "the" 뒤에 하나 이상의 단어와 일치한다.

캡처는 그룹의 일치된 값을 나중에 사용할 수 있도록 저장하는 방법이다. `match` 객체 메서드와 속성을 사용하여 캡처된 그룹에 액세스할 수 있다. 예를 들어 `match.group()` 메서드는 일치된 부분 문자열을 반환하고 `match.group(1)` 메서드는 첫 번째 캡처된 그룹을 반환한다.

구문 `(?P<name>pattern)`을 사용하여 그룹의 이름을 지정할 수도 있으며, 여기서 `name`은 그룹의 이름이고 `pattern`은 일치시킬 하위 패턴이다. 예를 들어 패턴 `(?P<animal>\w+) is ont the (?P<object>\w+)`은 몀몀된 2 그룹을 갖는다. "animal"이라는 이름의 그룹은 하나 이상의 단어와 일치하고 "object"라는 이름의 그룹은 "`the`"라는 단어 뒤에 하나 이상의 단어와 일치한다.

메서드 `match.group('name')` 또는 속성 `match.groupdict()`를 사용하여 캡처된 값을 액세스할 수 있다. 예를 들어 메서드 `match.group('animal')`은 "animal"이라는 이름의 그룹 값을 반환하고 속성 `match.groupdict()`는 이름이 지정된 모든 그룹과 해당 값의 사전을 반환한다.

Python에서 그룹과 캡처를 사용하는 예는 다음과 같다:

```python
# Import the re module
import re
# Define a string
text = "The cat is on the mat. The dog is on the log."
# Define a pattern with groups
pattern = "(\w+) is on the (\w+)"
# Search for the pattern in the string
result = re.search(pattern, text)
# Check if the pattern is found
if result:
    # Print the matched substring
    print(result.group())
    # Print the first group
    print(result.group(1))
    # Print the second group
    print(result.group(2))
    # Print the start and end positions of the match
    print(result.start(), result.end())
    # Print the span of the match
    print(result.span())
else:
    # Print a message if the pattern is not found
    print("No match found.")
# Output:
# cat is on the mat
# cat
# mat
# 4 19
# (4, 19)
# Define a pattern with named groups
pattern = "(?P\w+) is on the (?P\w+)"
# Search for the pattern in the string
result = re.search(pattern, text)
# Check if the pattern is found
if result:
    # Print the matched substring
    print(result.group())
    # Print the group named "animal"
    print(result.group('animal'))
    # Print the group named "object"
    print(result.group('object'))
    # Print the dictionary of named groups
    print(result.groupdict())
else:
    # Print a message if the pattern is not found
    print("No match found.")
# Output:
# cat is on the mat
# cat
# mat
# {'animal': 'cat', 'object': 'mat'}
```

## <a name="sec_07"></a> 플래그와 수식어
이 절에서는 Python에서 플래그와 수식어를 사용하여 정규 표현식의 동작을 제어하는 방법을 배울 것이다.

플래그와 수식어는 정규 표현식을 해석하거나 일치시키는 방식을 수정하는 특수 기호 또는 인수이다. 이들은 정규 표현식의 대소문자 구분, 다중 줄 모드, 도탈 모드(dotall mode), 동사 모드 및 유니코드 모드에 영향을 줄 수 있다.

플래그와 수식어는 두 가지 방법으로 사용할 수 있다. 즉, 플래그를 `re` 모듈 함수와 메서드에 인수로 전달하거나 (`?` 플래그) 구문을 사용하여 패턴에 포함시킨다. 여기서 플래그는 하나 이상의 플래그 문자이다.

다음은 가장 일반적인 플래그와 수식어의 의미를 나열한 목록이다.

![](./1_GFFHDXVesA99z2lgP9cWiQ.webp)

다음은 Python에서 플래그와 수식어를 사용하는 예이다.

```python
# Import the re module
import re
# Define a string
text = “The CAT is on the mat. The DOG is on the log.”
# Define a pattern
pattern = “cat”
# Search for the pattern in the string without flags
result = re.search(pattern, text)
# Check if the pattern is found
if result:
 # Print the matched substring
 print(result.group())
else:
 # Print a message if the pattern is not found
 print(“No match found.”)
# Output:
# No match found.
# Search for the pattern in the string with the IGNORECASE flag
result = re.search(pattern, text, flags=re.IGNORECASE)
# Check if the pattern is found
if result:
 # Print the matched substring
 print(result.group())
else:
 # Print a message if the pattern is not found
 print(“No match found.”)
# Output:
# CAT
```

## <a name="sec_08"></a> 고급 패턴과 기법
이 졸에서는 고급 정규 표현식 패턴을 작성하는 방법과 패턴을 보다 강력하고 유연하게 만들 수 있는 몇 가지 기술을 사용하는 방법을 배울 것이다.

Python에서 사용할 수 있는 고급 패턴과 기술은 다음과 같다.

![](./1_Jc3eZNxiStiXBgyPm6jWcQ.webp)

Python에서 고급 패턴과 기술을 사용하는 예는 다음과 같다.

```python
# Import the re module
import re
# Define a string
text = “The cat is on the mat. The dog is on the log. The rat is on the hat.”
# Define a pattern with backreferences
pattern = “(\w+) is on the \1”
# Find all matches of the pattern in the string
result = re.findall(pattern, text)
# Print the result
print(result)
# Output:
# [‘mat’, ‘log’]
# Define a pattern with lookaround assertions
pattern = “\w+(?=\.)”
# Find all matches of the pattern in the string
result = re.findall(pattern, text)
# Print the result
print(result)
# Output:
# [‘mat’, ‘log’, ‘hat’]
```

## <a name="sec_09"></a> 어플리케이션과 예
이 절에서는 Python에서 정규 표현식을 사용하는 어플리케이션과 예를 볼 수 있다. 프로그래밍과 데이터 분석에서 일반적인 문제와 작업을 해결하기 위해 정규 표현식을 사용하는 방법을 배우게 될 것이다.

일부 어플리케이션과 예는 다음과 같다.

이메일 주소 확인: 정규 표현식을 사용하여 지정된 문자열이 올바른 이메일 주소인지 확인할 수 있다. 유효한 이메일 주소는 사용자 이름, @ 기호, 도메인 이름과 선택적인 최상위 도메인으로 구성된다. 다음 패턴을 사용하여 유효한 이메일 주소를 일치시킬 수 있다,

```python
# Define a pattern for email validation
pattern = r”^[a-zA-Z0–9_.+-]+@[a-zA-Z0–9-]+\.[a-zA-Z0–9-.]+$”
# Define some test cases
email1 = “john.doe@example.com”
email2 = “jane_doe@example.co.uk”
email3 = “invalid@domain”
email4 = “no.at.symbol”
# Import the re module
import re
# Check if the test cases match the pattern
print(re.match(pattern, email1)) # Match object
print(re.match(pattern, email2)) # Match object
print(re.match(pattern, email3)) # None
print(re.match(pattern, email4)) # None
```

전화 번호 추출: 텍스트에서 전화 번호를 추출하기 위해 정규 표현식을 사용할 수 있다. 전화 번호는 (123) 456–7890, (123) 456–7890, (123–456–7890) 또는 (1234567890)과 같은 다양한 형식을 가질 수 있다. 다음과 같은 형식을 사용하여 일치시킬 수 있다.

```python
# Define a pattern for phone number extraction
pattern = r”\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})”
# Define a text
text = “Call me at (123) 456–7890 or 123–456–7890 or 1234567890.”
# Import the re module
import re
# Find all matches of the pattern in the text
result = re.findall(pattern, text)
# Print the result
print(result)
# Output:
# [(‘123’, ‘456’, ‘7890’), (‘123’, ‘456’, ‘7890’), (‘123’, ‘456’, ‘7890’)]
```

텍스트 대체: 정규 표현식을 사용하여 문자열의 텍스트를 새 텍스트 또는 함수로 대체할 수 있다. 예를 들어 다음 코드를 사용하여 모든 문자열 "cat"을 문자열 "dog"로 대체할 수 있다.

```python
# Define a string
text = “The cat is on the mat. The cat is cute.”
# Define a pattern
pattern = “cat”
# Define a replacement
repl = “dog”
# Import the re module
import re
# Replace the pattern with the replacement in the string
result = re.sub(pattern, repl, text)
# Print the result
print(result)
# Output:
# The dog is on the mat. The dog is cute.
```

텍스트 분할: 정규 표현식을 사용하여 문자열을 분할할 수 있다. 예를 들어, 다음 코드를 사용하여 문장 부호로 문자열을 분할할 수 있다.

```python
# Define a string
text = “Hello, world! How are you?”
# Define a pattern
pattern = r”[,.?!]”
# Import the re module
import re
# Split the string by the pattern
result = re.split(pattern, text)
# Print the result
print(result)
# Output:
# [‘Hello’, ‘ world’, ‘ How are you’, ‘’]
```

이들은 Python에서 정규 표현식을 사용하는 방법을 보여주는 몇몇 예에 불과하다. 정규 표현식을 사용하여 탐색할 수 있는 어플리케이션과 가능성은 훨씬 더 많다. 모듈 설명서와 온라인 regex 테스터를 사용하여 정규 표현식에 대해 자세히 알아보고 기술을 연습할 수 있다.

[다음 절](#summary)에서는 이 포스팅을 마무리하고 추가 학습을 위한 리소스를 제공하고자 한다.

## <a name="summary"></a> 요약
수고하셨습니다! Python 정규 표현식에 대한 이 포스팅의 끝에 도달했다. Python에서 패턴 매칭과 텍스트 조작을 위해 정규 표현식을 사용하는 방법을 배웠다.

이 포스팅의 내용을 요약하면 다음과 같다.

- 정규 표현식은 무엇이며 작동 방식은 어떠한가
- Python에서 `re` 모듈을 사용하는 방법
- 기본과 고급 정규 표현 패턴 작성 방법
- 모듈의 일반적인 메서드와 속성을 사용하는 방법
- 그룹과 캡처를 사용하여 하위 일치를 추출하는 방법
- 플래그와 수식어를 사용하여 정규 표현식의 동작을 제어하는 방법
- 다양한 작업과 문제에 정규 표현식을 적용하는 방법
